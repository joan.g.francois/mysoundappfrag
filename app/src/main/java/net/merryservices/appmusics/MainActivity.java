package net.merryservices.appmusics;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import net.merryservices.appmusics.database.FavoriteRepository;
import net.merryservices.appmusics.fragment.MusicFragment;
import net.merryservices.appmusics.fragment.SearchFragment;
import net.merryservices.appmusics.model.IOnSelectedMusic;
import net.merryservices.appmusics.model.Music;
import net.merryservices.appmusics.model.MusicAdapter;
import net.merryservices.appmusics.service.IListenerAPI;
import net.merryservices.appmusics.service.ServiceApi;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements IOnSelectedMusic {

    private SearchFragment searchFragment;
    private MusicFragment musicFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (isMobile()) {
            searchFragment = new SearchFragment();
            musicFragment = new MusicFragment();

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.frameLayout, searchFragment)
                    .add(R.id.frameLayout, musicFragment)
                    .hide(musicFragment)
                    .commit();
        }else{
            searchFragment = (SearchFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentSearch);
            musicFragment  = (MusicFragment)  getSupportFragmentManager().findFragmentById(R.id.fragmentMusic);
            getSupportFragmentManager().beginTransaction()
                    .hide(musicFragment)
                    .commit();
        }
        searchFragment.setListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==R.id.menuItemFavoris){
            if (isMobile()) {
                getSupportFragmentManager().beginTransaction()
                        .hide(musicFragment)
                        .show(searchFragment)
                        .commit();
            }
            searchFragment.showFavorites();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSelectedMusic(Music music) {
        if (isMobile()) {
            getSupportFragmentManager().beginTransaction()
                    .hide(searchFragment)
                    .show(musicFragment)
                    .commit();
        }else{
            getSupportFragmentManager().beginTransaction()
                    .show(musicFragment)
                    .commit();
        }
        musicFragment.setCurrentMusic(music);
    }

    @Override
    public void onBackPressed() {
        if(isMobile() && musicFragment.isVisible()) {
            getSupportFragmentManager().beginTransaction()
                    .hide(musicFragment)
                    .show(searchFragment)
                    .commit();
            searchFragment.refresh();
        }else{
            super.onBackPressed();
        }
    }

    private boolean isMobile(){
        return findViewById(R.id.frameLayout)!=null;
    }
}