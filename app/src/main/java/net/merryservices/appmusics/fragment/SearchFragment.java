package net.merryservices.appmusics.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import net.merryservices.appmusics.R;
import net.merryservices.appmusics.database.FavoriteRepository;
import net.merryservices.appmusics.model.IOnSelectedMusic;
import net.merryservices.appmusics.model.Music;
import net.merryservices.appmusics.model.MusicAdapter;
import net.merryservices.appmusics.service.IListenerAPI;
import net.merryservices.appmusics.service.ServiceApi;

import java.util.ArrayList;

public class SearchFragment extends Fragment implements AdapterView.OnItemClickListener, IListenerAPI, View.OnClickListener {

    private ListView listViewMusics;
    private EditText editTextSearch;
    private ImageButton buttonSearch;
    private ArrayList<Music> musics;
    private IOnSelectedMusic listener;

    public void setListener(IOnSelectedMusic listener) {
        this.listener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_search, null);
        listViewMusics = v.findViewById(R.id.listViewMusics);
        listViewMusics.setOnItemClickListener(this);
        showFavorites();
        editTextSearch= v.findViewById(R.id.editTextSearch);
        buttonSearch= v.findViewById(R.id.imageButtonSearch);
        buttonSearch.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View v) {
        String search= editTextSearch.getText().toString();
        ServiceApi.musicsRequest(getContext(), search, this);
        editTextSearch.clearFocus();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        listener.onSelectedMusic(musics.get(position));
    }

    @Override
    public void onReceiveMusics(ArrayList<Music> musics) {
        this.musics= musics;
        refresh();
    }

    public void showFavorites(){
        this.musics = FavoriteRepository.getInstance(getContext()).getAll();
        refresh();
    }

    public void refresh(){
        MusicAdapter musicAdapter= new MusicAdapter(getActivity(), musics);
        listViewMusics.setAdapter(musicAdapter);
    }
}